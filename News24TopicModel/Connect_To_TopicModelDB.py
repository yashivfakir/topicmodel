

import pymysql.cursors

#The database is a amazon RDS DB, There are seven tables of which one is a manually entered data,
# The Log in details for the DB is stored in the same folder as the this module called: "TopicModelDB_LogInData.txt"

counter=0# used in the iteration retrieval process

# Connect to the database
connection = pymysql.connect(host='topicmodeldb.cojsryqvcndw.eu-west-1.rds.amazonaws.com',
                             user='TopicModDB_Acces', #This is not a typo error for the word Access
                             password='Password2',
                             db='TopicModelDataBase',
                            charset = 'utf8mb4',
                            cursorclass = pymysql.cursors.DictCursor)

#Adds a LDA model generated Topic to the LDA Topics table  FOR TRAINING PURPOSES
def InsertTrainingTopicLDA(TopicLDA_2Darray):
    Table="Training_Data_LDA_Topics"
    IterationNumber = getNewIterationNumberTrainingLDA(Table)

    for i in TopicLDA_2Darray:   # the for loops are to iterate through the 2d array and ensure that a string is put in the DB and not a 1d array for each topic
        Topic_String=""
        for j in i:
            Topic_String=Topic_String+ j+", "


        try:
            with connection.cursor() as cursor:
                # Create a new record
                sql = "INSERT INTO `Training_Data_LDA_Topics` (`LDA_Topic`, `IterationNumber`) VALUES ( %s, %s)"
                cursor.execute(sql, (Topic_String, IterationNumber))

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
        finally:
            connection.close()

#Adds a NMF model generated Topic to the LDA Topics table   FOR TRAINING PURPOSES
def InsertTrainingTopicNMF(TopicNMF):
    Table="Training_Data_NMF_Topics"
    IterationNumber = getNewIterationNumberTrainingLDA(Table)
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO `Training_Data_NMF_Topics` (`NMF_Topic`, `IterationNumber`) VALUES ( %s, %s)"
            cursor.execute(sql, (TopicNMF, IterationNumber))

        # connection is not autocommit by default. So you must commit to save
        # your changes.
        connection.commit()
    finally:
        connection.close()


# Adds a LDA model generated Topic to the LDA Topics table  FOR EVALUATION PURPOSES
def InsertEvaluationTopicLDA(TopicLDA_2Darray):
    Table = "Evaluation_Data_LDA_Topics"
    IterationNumber = getNewIterationNumberTrainingLDA(Table)

    for i in TopicLDA_2Darray:   # the for loops are to iterate through the 2d array and ensure that a string is put in the DB and not a 1d array for each topic
        Topic_String=""
        for j in i:
            Topic_String=Topic_String+ j+", "
        try:
            with connection.cursor() as cursor:
                # Create a new record
                sql = "INSERT INTO `Evaluation_Data_LDA_Topics` (`LDA_Topic`, `IterationNumber`) VALUES ( %s, %s)"
                cursor.execute(sql, (Topic_String, IterationNumber))

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
        finally:
            connection.close()


#Adds a NMF model generated Topic to the LDA Topics table   FOR TRAINING PURPOSES
def InsertEvaluationTopicNMF(TopicNMF_2Darray):
    Table="Evaluation_Data_NMF_Topics"
    IterationNumber=getNewIterationNumberTrainingLDA(Table)
    for i in TopicNMF_2Darray:   # the for loops are to iterate through the 2d array and ensure that a string is put in the DB and not a 1d array for each topic
        Topic_String=""
        for j in i:
            Topic_String=Topic_String+ j+", "
    try:
        with connection.cursor() as cursor:
            # Create a new record
            sql = "INSERT INTO `Training_Data_NMF_Topics` (`NMF_Topic`, `IterationNumber`) VALUES ( %s, %s)"
            cursor.execute(sql, (Topic_String, IterationNumber))

        # connection is not autocommit by default. So you must commit to save
        # your changes.
        connection.commit()
    finally:
        connection.close()


#Getting the previous number of times that the model was run
def getNewIterationNumberTrainingLDA(Table):
    global counter
    modelIterationNumber=0

    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `IterationNumber` FROM `"+Table+"` ORDER BY IterationNumber DESC LIMIT 1"

            cursor.execute(sql)
            result = cursor.fetchone()
            if result==None:
                modelIterationNumber=1
                counter=counter+1
            elif counter%10==0:
                modelIterationNumber=int(result)+1
                counter=1
            else:
                modelIterationNumber=int(result)
                counter=counter+1

    finally:
        connection.close()
        return modelIterationNumber


#gets the Evaluation data to be run on each model for training purposes
def getEvaluationData():
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `EvaluationHeadlines` FROM `Evaluation_Data_Headlines`"

            cursor.execute(sql)
            result = cursor.fetchall()


    finally:
        connection.close()
        return result   #Result is an array with a dictionary representing every headline

#gets the Evaluation data to be run on each model for training purposes
def getTraininingData():
    try:
        with connection.cursor() as cursor:
            # Read a single record
            sql = "SELECT `TrainingHeadlines` FROM `Training_Data_Headlines`"

            cursor.execute(sql)
            result = cursor.fetchall()


    finally:
        connection.close()
        return result   #Result is an array with a dictionary representing every headline


#Used to add more article headlines to the trainging set
def IncreaseTrainingDataSet(InitialArray):
    #InitialArray should be a a 1D array with each element being a string to represent the headline
    try:
        for i in InitialArray:
            with connection.cursor() as cursor:
                # Create a new record
                sql = "INSERT INTO `Training_Data_Headlines` (`TrainingHeadlines`) VALUES ( %s)"
                cursor.execute(sql, (i))

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
    finally:
        connection.close()

#Used to add more article headlines to the evaluation set
def IncreaseEvaluationDataSet(InitialArray):
    #InitialArray should be a a 1D array with each element being a string to represent the headline
    try:
        for i in InitialArray:
            with connection.cursor() as cursor:
                # Create a new record
                sql = "INSERT INTO `Evaluation_Data_Headlines` (`EvaluationnHeadlines`) VALUES ( %s)"
                cursor.execute(sql, (i))

            # connection is not autocommit by default. So you must commit to save
            # your changes.
            connection.commit()
    finally:
        connection.close()

