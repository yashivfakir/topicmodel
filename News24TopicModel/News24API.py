import requests

#gets articles from News24
url =('https://newsapi.org/v2/everything?sources=news24&apiKey=ad7286dd5ed44137a5e83fd1995050d0')

ArticleData= requests.get(url).json()


#Empty array to store headlines from the articles
ArticleTitlesArray=[len(ArticleData['articles'])]


def getArticleTitles(RawDataArray, getTerm):

    for x in range(0,len(ArticleData['articles'])):
        temp = ArticleData['articles'][x]
        #used encoding to remove weird symbols that were extracted from the News24 API
        newTemp = temp.get(getTerm).encode('utf-8').strip()

        RawDataArray.append(newTemp.decode('utf-8'))
    return RawDataArray


ArticleTitlesArray=getArticleTitles(ArticleTitlesArray, "title")
#RawDataArray=getData(RawDataArray,"description")

#index [0] is the length and am thus removing it AND removed the preTitle being "News24.com |........heading....."
del ArticleTitlesArray[0]
HeadLineArray=[]
for i in ArticleTitlesArray:
    prefix= i[0:12]
    if prefix.find("News24")==-1:
        HeadLineArray.append(i)
    else:
        i=i[12:len(i)]
        HeadLineArray.append(i)


#function to retrieve HeadlineArray
def getArray():
    return HeadLineArray


