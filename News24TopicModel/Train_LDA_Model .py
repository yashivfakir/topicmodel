from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
import gensim
from gensim import corpora

########################################################################################################################

# Methods for later

########################################################################################################################

#outputs each of the ten topics as a list in a single list called TopicArray after running the model....................
def createTopicArray(ldamodel):
    StringTopicsArray=[]
    for index, topic in ldamodel.show_topics(formatted=False, num_words=10):
        StringTopicsArray.append(format( [w[0] for w in topic]))
    TopicArray=[]
    count=0
    for x in StringTopicsArray:
        x=x[1:len(x)-1]
        temp=x.split(",")
        TopicArrayElement = []
        for y in temp:
            ind=y.index("'")
            y=y[ind+1:len(y)-1]
            TopicArrayElement.append(y)
        TopicArray.append(TopicArrayElement)

   # TopicArray=json.dumps(TopicArray)

    return TopicArray

#Writes the topics to "Topics.txt" to be used by the UI to display in a checkbox........................................
#Didn't end up using this but left it here incase needed later
def wrtiteToTXT(array):
    f=open("Topics.txt","w")
    for i in array:
        for j in i:
            f.write(j+",")
        f.write('\n')
    f.write("Working")

    f.close()

########################################################################################################################

# Static Training Data

########################################################################################################################

InitialArray=[' Briton arrested in Egypt for spying charges says he is free',
              ' UN: 20 million Yemenis are hungry, 250 000 face catastrophe',
              ' WATCH: Durban metro cop loses her cool with motorist',
              ' VBS saga: Radzilani resigns, 6 other Limpopo mayors told to go',
              " SA seeks normalised ties with Rwanda after Lindiwe Sisulu 'prostitute' insult",
              ' Pule Mabe takes leave, pending outcome of sexual harassment inquiry',
              ' Why Pravin Gordhan is wrong about Eskom',
              ' ANCYL branches join calls for Mabe to go',
              ' UN says Islamic State kills 6 captives in Libya',
              ' Law society condemns attack on female advocate',
              " Omotoso's lawyers preparing to go to ConCourt",
              ' WATCH: Monkeys terrorise India’s centre of power',
              " Pule Mabe sexual harassment accusations: Hawks 'get ready' to investigate",
              " SCA dismisses Omotoso's bid to have judge recuse himself - trial to go ahead",
              ' To achieve universal healthcare, Kenya must invest more in its nurses',
              " 'We aren't interested in coalition talks with opposition,' says Zim VP",
              ' Qedani Mahlangu, Brian Hlongwa told to step down from ANC Gauteng PEC',
              ' Ensure load shedding stops – alliance political council',
              ' ANCWL calling for Pule Mabe’s suspension following sexual harassment allegations',
              " Macron unveils new measures in bid to end 'yellow vest' revolt",

              "Ramaphosa pleased by Zuma's presence at ANC's 107th celebrations",
              "Alaska guide pleads guilty to using employees on snowmobiles to herd grizzly bears toward clients",
              "DRC opposition candidate warns against 'disguising truth' of election",
              "8 suspects wanted for serious crimes arrested by Ekurhuleni police",
              "State capture must be probed, can't be swept 'under the carpet' - Mbalula",
              "AU, UN to revive Central African Republic peace talks",
              "DRC election results could be counted in '24-48 hours': official",
              "Musician charged in family slayings seeks insanity plea",
              "LIVE: Ready, set, go! First day of school for 2019 begins",
              "Police arrest suspect in bombing of a cash-in-transit vehicle that left EMPD officer wounded",
              "Durban teen set for Special Olympics in UAE",
              "Trump pleads on TV for wall money; Dems say he 'stokes fear'",
              "US woman sentenced to life as teen in killing wins clemency",
              "LATEST: DRC police gather outside electoral commission",
              "Thousands protest al-Bashir's rule in eastern Sudanese city",
              "This top Khayelitsha matriculant walked two hours from school, cooked supper and then did her homework"
              ]
########################################################################################################################

           #Cleans array

########################################################################################################################

#Removes prefix labels e.g. "Breaking News: blah blah blah" or "Watch Now: blah blah blah"..............................
Array=[]
for x in InitialArray:
    dash=x.find("-")
    colon=x.find(":")
    if x.find("-")==-1 and colon==-1:
        Array.append(x)
    elif dash==-1:
        capitals = x[0:colon]
        space=x[colon+1]
        if capitals.isupper() and colon < 20 and space==" ":
            x = x[colon + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)
    elif colon==-1:
        space = x[dash + 1]
        capitals = x[0:dash]
        if capitals.isupper() and dash < 20 and space==" ":
            x = x[dash + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)

#Cleans the Array of redundant words....................................................................................

stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

CleanedArray = [clean(doc).split() for doc in Array]


########################################################################################################################

        #Runs the Model

########################################################################################################################

# Creating the term dictionary of our courpus, where every unique term is assigned an index.
dictionary = corpora.Dictionary(CleanedArray)
# Converting list of Array (corpus) into Array Term Matrix using dictionary prepared above.
doc_term_matrix = [dictionary.doc2bow(doc) for doc in CleanedArray]

#Running LDA Model......................................................................................................
#from gensim.test.utils import datapath
# Running and Training LDA model on the Array term matrix.

temp_file="LDA_Topic_Model"
#temp_file
Lda = gensim.models.ldamodel.LdaModel

#loads previous model...................................................................................................
ldamodel=Lda.load(temp_file)
ldamodel = Lda(doc_term_matrix, num_topics=10, id2word = dictionary, passes=500)

#Saves the model after fitting and modyfiying new data..................................................................
ldamodel.save('LDA_Topic_Model')

if __name__ == "__main__":
    array = createTopicArray(ldamodel)
    for i in array:
        print(i)









