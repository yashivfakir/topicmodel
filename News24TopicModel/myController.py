from flask import Flask
import Implement_LDA_Model
app = Flask(__name__)


@app.route("/")
def getTopics():
    return Implement_LDA_Model.getArray()
if __name__ == "__main__":
    app.run(debug=True)
