Ensure that python 3 or higher is installed NOT 2.7

Tweets:	
	Twitter_streaming.py:	
	This streams live FILTERED data from the twitter API but is unable to stop the program.
	because the program cannot stop the tweets are never saved to the twitter_data.txt (but if able to stop the code to save the tweets is there already)
	
	Install: "pip install tweepy"

Topic Models:

	There are two topic models: The LDA and NMF models
		
	For: LDA
	Modules: Training_Model_LDA.py and Evaluate_LDA_Model.py
	The training module, the module topic articles from a AWS database trains and saves the model
	The evaluate module is used to test the saved model's accuracy
	The evaluate module can also be used to filter live article headlines and generate topics
        
	Install for lda: "pip install nltk" and "pip install gensim" in the root of
	
	For: NMF
	Modules: Training_Model_NMF.py and Evaluate_NMF_Model.py
	the training module creates and trains a model but is unable to save the model and thus 
	the eval module cannot yet be used to test the accuracy
 
Connecting to Database:
	
	Module: Connect_To_TopicModelDB.py
	there are functions already installed to write and recieve data from the database to be used in other classes.
	Simply import the module and uses the methods

	Note. all log in data for the DB is stored in TopicModelDB_LogInData.txt in the same root file as this .txt folder

News24API

	Module: News24API.py
	This streams 20 topics live from News24 to be used by the models

	Install: "pip install requests"

HeatMap:
	the HeatMap.py was just a attempt at generating a heatmap with no related data 
	but never ended up using the module


BitBucket:
	On bitbucket name:SocialSentimentAnalysis2 is the repo with all modules and files
	There are two branchs: 1) yashiv: has all modules related to the Topic models
			       2) master: this has all files for the Jhipster app for the UI of the analyser

For the User Interface(UI):
	IGNORE: JHipsterUI NB NB NB NB!!!!!
	name: "SentimentUI"
	In terminal go into the SentimentUI directory and type command "npm start" or if yarn is installed "yarn run start"
	The above command will generate the UI and then enable broswer hot syncing to allow for editiing of the UI
	Angular should aleady be installed along with all imports for the graphing software
	

	Outline of UI directory:
		All files for the front end bit are stored in ".../src/main/webapp.app"
		Individual components are then further stored in ".../layouts"
		The main.component.html lays out the components on their respective divs.
		
		The graph component is stored in ".../app/home" 
		The tweets are streamed from ".../app/tweets"
 

