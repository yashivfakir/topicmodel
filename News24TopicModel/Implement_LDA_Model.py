
import News24API
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string
import gensim
from gensim import corpora

########################################################################################################################

        #Methods for later

########################################################################################################################

#outputs each of the ten topics as a list in a single list called TopicArray............................................
def createTopicArray(ldamodel):
    StringTopicsArray=[]
    for index, topic in ldamodel.show_topics(formatted=False, num_words=10):
        StringTopicsArray.append(format( [w[0] for w in topic]))
    TopicArray=[]
    count=0
    for x in StringTopicsArray:
        x=x[1:len(x)-1]
        temp=x.split(",")
        TopicArrayElement = []
        for y in temp:
            ind=y.index("'")
            y=y[ind+1:len(y)-1]
            TopicArrayElement.append(y)
        TopicArray.append(TopicArrayElement)

   # TopicArray=json.dumps(TopicArray)

    return TopicArray

#Writes the topics to "Topics.txt" to be used by the UI to display in a checkbox........................................
#never ended up implementing method
def wrtiteToTXT(array):
    f=open("Topics.txt","w")
    for i in array:
        for j in i:
            f.write(j+",")
        f.write('\n')
    f.write("Working")

    f.close()

########################################################################################################################

           #Get live News24 data from News24 website

########################################################################################################################

InitialArray= News24API.getArray()

########################################################################################################################

           #Cleans array

########################################################################################################################

#Removes prefix labels e.g. "Breaking News: blah blah blah" or "Watch Now: blah blah blah"..............................
Array=[]
for x in InitialArray:
    dash=x.find("-")
    colon=x.find(":")
    if x.find("-")==-1 and colon==-1:
        Array.append(x)
    elif dash==-1:
        capitals = x[0:colon]
        space=x[colon+1]
        if capitals.isupper() and colon < 20 and space==" ":
            x = x[colon + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)
    elif colon==-1:
        space = x[dash + 1]
        capitals = x[0:dash]
        if capitals.isupper() and dash < 20 and space==" ":
            x = x[dash + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)




#Cleans the Array of redundant words....................................................................................


stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

CleanedArray = [clean(doc).split() for doc in Array]


########################################################################################################################

        #Runs the data on the loaded model

########################################################################################################################


#Preparing Array-Term Matrix............................................................................................

# Creating the term dictionary of our courpus, where every unique term is assigned an index.
dictionary = corpora.Dictionary(CleanedArray)
# Converting list of Array (corpus) into Array Term Matrix using dictionary prepared above.
doc_term_matrix = [dictionary.doc2bow(doc) for doc in CleanedArray]

#Running LDA Model......................................................................................................
#from gensim.test.utils import datapath
# Running and Training LDA model on the Array term matrix.

#name of saved model
temp_file="LDA_Topic_Model"

Lda = gensim.models.ldamodel.LdaModel
#loads the previously created model.....................................................................................
ldamodel=Lda.load(temp_file)
ldamodel = Lda(doc_term_matrix, num_topics=10, id2word = dictionary, passes=500)


array = createTopicArray(ldamodel)

for i in array:
    print(i)










