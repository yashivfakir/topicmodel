from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.preprocessing import normalize
import pickle
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

########################################################################################################################

# Methods for later

########################################################################################################################

#Method to return topics as an Array....................................................................................
def get_nmf_topics(model, n_top_words):
    # the word ids obtained need to be reverse-mapped to the words so we can print the topic names.
    feat_names = vectorizer.get_feature_names()
    TopicArray= []

    for i in range(num_topics):
        # for each topic, obtain the largest values, and add the words they map to into the dictionary.
        words_ids = model.components_[i].argsort()[:-10 - 1:-1]
        words = [feat_names[key] for key in words_ids]
        TopicArray.append(words)

    return TopicArray


########################################################################################################################

# Static Training data

########################################################################################################################

InitialArray=[' Briton arrested in Egypt for spying charges says he is free',
              ' UN: 20 million Yemenis are hungry, 250 000 face catastrophe',
              ' WATCH: Durban metro cop loses her cool with motorist',
              ' VBS saga: Radzilani resigns, 6 other Limpopo mayors told to go',
              " SA seeks normalised ties with Rwanda after Lindiwe Sisulu 'prostitute' insult",
              ' Pule Mabe takes leave, pending outcome of sexual harassment inquiry',
              ' Why Pravin Gordhan is wrong about Eskom',
              ' ANCYL branches join calls for Mabe to go',
              ' UN says Islamic State kills 6 captives in Libya',
              ' Law society condemns attack on female advocate',
              " Omotoso's lawyers preparing to go to ConCourt",
              ' WATCH: Monkeys terrorise India’s centre of power',
              " Pule Mabe sexual harassment accusations: Hawks 'get ready' to investigate",
              " SCA dismisses Omotoso's bid to have judge recuse himself - trial to go ahead",
              ' To achieve universal healthcare, Kenya must invest more in its nurses',
              " 'We aren't interested in coalition talks with opposition,' says Zim VP",
              ' Qedani Mahlangu, Brian Hlongwa told to step down from ANC Gauteng PEC',
              ' Ensure load shedding stops – alliance political council',
              ' ANCWL calling for Pule Mabe’s suspension following sexual harassment allegations',
              " Macron unveils new measures in bid to end 'yellow vest' revolt",

              "Ramaphosa pleased by Zuma's presence at ANC's 107th celebrations",
              "Alaska guide pleads guilty to using employees on snowmobiles to herd grizzly bears toward clients",
              "DRC opposition candidate warns against 'disguising truth' of election",
              "8 suspects wanted for serious crimes arrested by Ekurhuleni police",
              "State capture must be probed, can't be swept 'under the carpet' - Mbalula",
              "AU, UN to revive Central African Republic peace talks",
              "DRC election results could be counted in '24-48 hours': official",
              "Musician charged in family slayings seeks insanity plea",
              "LIVE: Ready, set, go! First day of school for 2019 begins",
              "Police arrest suspect in bombing of a cash-in-transit vehicle that left EMPD officer wounded",
              "Durban teen set for Special Olympics in UAE",
              "Trump pleads on TV for wall money; Dems say he 'stokes fear'",
              "US woman sentenced to life as teen in killing wins clemency",
              "LATEST: DRC police gather outside electoral commission",
              "Thousands protest al-Bashir's rule in eastern Sudanese city",
              "This top Khayelitsha matriculant walked two hours from school, cooked supper and then did her homework"
              ]

########################################################################################################################

# Data Cleaning

########################################################################################################################

#Removes prefix labels e.g. "Breaking News: blah blah blah" or "Watch Now: blah blah blah"..............................
Array=[]
for x in InitialArray:
    dash=x.find("-")
    colon=x.find(":")
    if dash==-1 and colon==-1:
        Array.append(x)
    elif dash==-1:
        capitals = x[0:colon]
        space=x[colon+1]
        if capitals.isupper() and colon < 20 and space==" ":
            x = x[colon + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)
    elif colon==-1:
        space = x[dash + 1]
        capitals = x[0:dash]
        if capitals.isupper() and dash < 20 and space==" ":
            x = x[dash + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)

#Cleans the Array of redundant words....................................................................................
stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

CleanedArray = [clean(doc).split() for doc in Array]


#Joins the useful idndividual words after previous method split them....................................................
train_headlines_sentences = [' '.join(text) for text in CleanedArray]

#obtain a Counts design matrix.
vectorizer = CountVectorizer(analyzer='word', max_features=500)
x_counts = vectorizer.fit_transform(train_headlines_sentences)

num_topics=10

#set a TfIdf transformer, and transfer the counts with the model.
transformer = TfidfTransformer(smooth_idf=False)
x_tfidf = transformer.fit_transform(x_counts)
#normalize the TfIdf values so each row has unit length.
xtfidf_norm = normalize(x_tfidf, norm='l1', axis=1)

########################################################################################################################

# Loads, Runs and saves the model

########################################################################################################################

#obtain a NMF model.
loaded_model = pickle.load(open('NMF_Topic_Model.pkl', 'rb'))

#fit the model
loaded_model.fit_transform(xtfidf_norm)

#Saves the created model

filename = 'NMF_Topic_Model.pkl'
pickle.dump(loaded_model, open(filename, 'wb'))

#Calls the array function
if __name__ == "__main__":
    TopicArray = get_nmf_topics(loaded_model, 10)
    for i in TopicArray:
        print(i)
