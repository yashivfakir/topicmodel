from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer
from sklearn.preprocessing import normalize
import pickle
import News24API
from nltk.corpus import stopwords
from nltk.stem.wordnet import WordNetLemmatizer
import string

########################################################################################################################

# Methods for later

########################################################################################################################

#Method to return topics as an Array
def get_nmf_topics(model, n_top_words):
    # the word ids obtained need to be reverse-mapped to the words so we can print the topic names.
    feat_names = vectorizer.get_feature_names()

    TopicArray= []

    for i in range(num_topics):
        # for each topic, obtain the largest values, and add the words they map to into the dictionary.
        words_ids = model.components_[i].argsort()[:-10 - 1:-1]

        words = [feat_names[key] for key in words_ids]
        TopicArray.append(words)

    return TopicArray


########################################################################################################################

# Gets article headlines from news 24

########################################################################################################################


InitialArray= News24API.getArray()

########################################################################################################################

# Cleans data

########################################################################################################################

#Removes prefix labels e.g. "Breaking News: blah blah blah" or "Watch Now: blah blah blah"
Array=[]
for x in InitialArray:
    dash=x.find("-")
    colon=x.find(":")
    if dash==-1 and colon==-1:
        Array.append(x)
    elif dash==-1:
        capitals = x[0:colon]
        space=x[colon+1]
        if capitals.isupper() and colon < 20 and space==" ":
            x = x[colon + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)
    elif colon==-1:
        space = x[dash + 1]
        capitals = x[0:dash]
        if capitals.isupper() and dash < 20 and space==" ":
            x = x[dash + 1:len(x)]
            Array.append(x)
        else:
            Array.append(x)




#Cleans the Array of redundant words.........................................................................................


stop = set(stopwords.words('english'))
exclude = set(string.punctuation)
lemma = WordNetLemmatizer()
def clean(doc):
    stop_free = " ".join([i for i in doc.lower().split() if i not in stop])
    punc_free = ''.join(ch for ch in stop_free if ch not in exclude)
    normalized = " ".join(lemma.lemmatize(word) for word in punc_free.split())
    return normalized

CleanedArray = [clean(doc).split() for doc in Array]


#Reconnects words after redundant words removed
train_headlines_sentences = [' '.join(text) for text in CleanedArray]

#obtain a Counts design matrix.
vectorizer = CountVectorizer(analyzer='word', max_features=500)
x_counts = vectorizer.fit_transform(train_headlines_sentences)

num_topics=10

#set a TfIdf transformer, and transfer the counts with the model.
transformer = TfidfTransformer(smooth_idf=False)
x_tfidf = transformer.fit_transform(x_counts)
#normalize the TfIdf values so each row has unit length.
xtfidf_norm = normalize(x_tfidf, norm='l1', axis=1)

########################################################################################################################

# Loads and runs the model on new data

########################################################################################################################

#obtain a NMF model.
loaded_model = pickle.load(open('NMF_Topic_Model.pkl', 'rb'))

#fit the model
loaded_model.fit_transform(xtfidf_norm)

#Calls the array function
TopicArray=get_nmf_topics(loaded_model, 10)



